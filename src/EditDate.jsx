import React from 'react';

const EditDate = props => {
    return (

        <div className='edit-date'>
            <div className='edit-date__input-group'>
            <input onChange={e => props.onInputChange({[e.target.value]:''})} type="text"/>
            </div>
        </div>
    )
    }

export default EditDate;