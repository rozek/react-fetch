import React, { Component } from 'react';
import './App.css';
import Yearinfo from './Yearinfo';

class App extends Component {

  state ={
    text: 'W tym roku pokonalismy krzyzakow',
    error:''
  }



handleDateChange = () => {
  const value = this.refs.number.value;
  console.log(value);
  fetch(`http://numbersapi.com/${value}/year?json`)
  //Response
  .then(res => {
    if (res.ok) {
      return res
    }
    throw Error('Cos jest nie tak');
  })
  .then(res => res.json())
  .then(data => this.setState({
    text: data.text
  }))
  .catch(err => console.log(err)
  )
}

  render() {
    const yearinfo =this.state.text
    return (
    <div>
    <input onChange={this.handleDateChange} type='text' ref='number'/>
      <Yearinfo text={yearinfo}/>
    </div>)
        
  }
}


export default App;
